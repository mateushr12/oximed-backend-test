//require("dotenv-safe").config();

//const jwt = require('jsonwebtoken');
//const db = require("./config/db");

const http = require('http'); 
const express = require('express'); 
const app = express(); 
 
const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

// app.get('/', (req, res, next) => {
//     res.json({message: "Tudo ok por aqui!"});
// })

//const md5 = require('md5');

//const indexrouter = require('./router/index');
const loginRouter = require('./src/router/route');
app.use("/api",loginRouter);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
 
