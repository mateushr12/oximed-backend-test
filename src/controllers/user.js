require("dotenv-safe").config();

const pool = require("../src/config/db");
const md5 = require('md5');
const jwt = require('jsonwebtoken');

//metodo para logar
exports.login = async(req, res) => {
	const { login, password } = req.body;
 	try {
    	const user = await pool.query("SELECT * FROM tb_user WHERE login = $1", [
      		login
    	]);

    	if (user.rows.length === 0) {
      		return res.status(401).json("Usuario ñ encontrado");
    	}

    	const validPassword = md5(password);

    	if (user.rows[0].password != validPassword) {
      		return res.status(401).json("Senha invalida");
    	}
    	const id = (user.rows[0].id); 
    	const token = jwt.sign({ id }, process.env.SECRET, {
        	expiresIn: 7200 
    	});    
    	return res.json({ token });
  	} catch (err) {
    	console.error(err.message);
    	res.status(500).send("Server error");
  	}
};

//metodo para criar novo usuario
exports.novoUsuario = async(req, res) => {
	const {nome,login,email,cpf,password,grupo} = req.body;
	try{
		const md5password = md5(password);
	    let novoUser = await pool.query(
	      "insert into tb_user (nome,login,email,cpf,password,grupo_id) values($1, $2, $3, $4, $5, $6)",
	      [nome,login,email,cpf,md5password,grupo]
	    );
	    //const token = jwt.sign(novoUser.rows[0].id);
	    //return res.json({ token });
	    res.status(201).send({
    		message: "Novo usuario salvo com sucesso",
    		body: {
      			usuario: { nome,login,email,cpf,password,grupo }
    		},
  		});
	    //res.status(201).send(novoUser.rows)
	} catch (err) {
		console.error(err.message);
	    res.status(500).send(err.message);
	}
};


exports.usuarios = async(req, res) => {
	try{
		const usuarios = await pool.query("SELECT * FROM tb_user");
		res.status(200).send(usuarios.rows);			    	    	   
	}catch (err){
		console.error(err.message);
	    res.status(500).send("Erro no servidor");
	}
};