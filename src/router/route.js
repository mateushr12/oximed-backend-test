const express = require("express");
const router = express.Router();
//const pool = require("../config/db");
//const md5 = require('md5');
const verif = require('../src/middleware/verificacao');

//require("dotenv-safe").config();
//const jwt = require('jsonwebtoken');

const userController = require('../controllers/user');

router.post("/login", userController.login);

router.post("/logout", userController.logout);

router.post("/novoUsuario", verif ,userController.novoUsuario);

router.get("/usuarios", verif ,userController.usuarios);

module.exports = router;